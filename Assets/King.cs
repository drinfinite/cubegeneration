﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class King : MonoBehaviour
{
    private static int worldsize = 500;
    private static int npcQuantity = 300;
    private static float scarcityRatio = 2f; //there are 10 resources of each type to each NPC
    public GameObject NPC;
    public GameObject Food;
    public GameObject Water;
    public List<GameObject> population = new List<GameObject>();
    
    System.Random random = new System.Random();
    public int ticks = 0;

    // Start is called before the first frame update
    void Start()
    {
        SpawnGame();
    }

    public void Clone(float size, float aggressionRatio, float curiousityRatio, float huntRatio, float niceRatio, int generation, int age)
    { 
        int x = random.Next(1, worldsize - 1);
        int z = random.Next(1, worldsize - 1);
        GameObject peasant = Instantiate(NPC, new Vector3(x, 25, z), Quaternion.identity);
        NPCLogic npclogic = peasant.GetComponent<NPCLogic>();
        npclogic.aggressionRatio = aggressionRatio;
        npclogic.curiousityRatio = curiousityRatio;
        npclogic.huntRatio = huntRatio;
        npclogic.niceRatio = niceRatio;
        npclogic.generation = generation + 1;
        npclogic.size = size;

        System.IO.File.WriteAllText("D:/Data/NPCs/" + System.DateTime.UtcNow.Ticks + ".txt", 
            "Food:" + npclogic.food + "\nWater:" + npclogic.water + "\nAge:" + npclogic.age + "\nHealth:" + npclogic.health + "\nSize:" + npclogic.size + "\nAggression:" + aggressionRatio + "\nCuriousity:" + curiousityRatio + "\nGenerousity:" + niceRatio);

        population.Add(peasant);

        Debug.Log("Unit aged " + age + " was succeeded by unit of generation " + generation);
    }
    
    // Update is called once per frame
    void Update()
    {
        ticks++;

        if(ticks % 1000 == 0)
        {
            SpawnResources();
        }
    }

    void SpawnGame()
    {
        int x = 0;
        int z = 0;

        //Spawn NPCs
        for (int i = 0; i < npcQuantity; i++)
        {
            x = random.Next(1, worldsize - 1);
            z = random.Next(1, worldsize - 1);

            GameObject npc = Instantiate(NPC, new Vector3(x, 0, z), Quaternion.identity);

            NPCLogic logic = npc.GetComponent<NPCLogic>();
            logic.aggressionRatio = (float)random.NextDouble();
            logic.curiousityRatio = (float)random.NextDouble();
            logic.niceRatio = (float)random.NextDouble();
            logic.huntRatio = (float)random.NextDouble();

            population.Add(npc);
        }

    }

    void SpawnResources()
    {
        int x = 0;
        int z = 0;

        int max = (int)scarcityRatio * npcQuantity;
        GameObject[] waters = GameObject.FindGameObjectsWithTag("Water");
        GameObject[] foods = GameObject.FindGameObjectsWithTag("Food");

        for (int i = 0; i < max - waters.Length; i++)
        {
            x = random.Next(1, worldsize - 1);
            z = random.Next(1, worldsize - 1);

            Instantiate(Water, new Vector3(x, 15, z), Quaternion.identity);
        }

        for (int i = 0; i < max - foods.Length; i++)
        {
            x = random.Next(1, worldsize - 1);
            z = random.Next(1, worldsize - 1);

            Instantiate(Food, new Vector3(x, 15, z), Quaternion.identity);
        }
    }
}
