﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCLogic : MonoBehaviour
{
    /// <summary>
    /// Stats that change while alive
    /// </summary>
    public int food = 100;
    public int water = 100;
    public int age = 0; //Number of frames spent alive
    public int health = 100;
    public float size = 1;
    public string guid = "";

    /// <summary>
    /// Stats that are static and heritable
    /// </summary>
    public float aggressionRatio = 0.5f;
    public float curiousityRatio = 0.5f;
    public float huntRatio = 0.5f;
    public float niceRatio = 0.5f;
    public int generation = 0;

    /// <summary>
    /// General area
    /// </summary>
    public bool decisionRandomness = true;
    System.Random random = new System.Random();

    void Start()
    {
        if (aggressionRatio > curiousityRatio && aggressionRatio > huntRatio && aggressionRatio > niceRatio)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
        else if(curiousityRatio > huntRatio && curiousityRatio > niceRatio && curiousityRatio > aggressionRatio)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.black;
        }
        else if(huntRatio > curiousityRatio && huntRatio > aggressionRatio && huntRatio > niceRatio)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.blue;
        }

        //Size is for visuals, and effects speed
        gameObject.transform.localScale = new Vector3(size, size, size);

        //Make sure it doesn't clip through the ground on spawn
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + size, gameObject.transform.position.z);
    }

    void Update()
    {
        if(size < 0)
        {
            //game over
            Destroy(gameObject);
            Debug.Log("Permadeath occurred.");
        }

        int consumeRate = 1;
        if(size <= 30)
        {
            consumeRate = 31 - (int)size;
        }

        if(age % consumeRate == 0)
        {
            water--;
            if(age % (consumeRate+3) == 0)
            {
                food--;
            }
        }

        if(age % 1000 == 0)
        {
            size++; //reward for surviving quite a while
        }

        if(food <= 0 || water <= 0)
        {
            health--;
        }
        else if(food >= 100 || water >= 100)
        {
            food--;
            water--;
            size += 0.01f;
            gameObject.transform.localScale = new Vector3(size, size, size);
        }

        if (health < 0)
        {
            Die();
        }
        else
        {
            age++;

            //Decisions
            //For readability
            byte action = 0;


            if (decisionRandomness)
            {
                //Attack is move towards other NPC, if in range, attack
                double attack = random.NextDouble() * aggressionRatio;

                //Search is move towards nearest resource
                double search = random.NextDouble() * curiousityRatio;

                if (attack > search)
                {
                    action = 1;
                }

                //Hunt is move towards most needed resource
                double hunt = random.NextDouble() * huntRatio;

                if (hunt > attack && hunt > search)
                {
                    action = 2;
                }

                //Niceness is move towards other NPC, if in range, give them resources (regardless of if attacked)
                double nice = random.NextDouble() * niceRatio;

                if (nice > hunt && nice > attack && nice > search)
                {
                    action = 3;
                }
            }
            else
            {
                if (aggressionRatio > curiousityRatio && aggressionRatio > huntRatio && aggressionRatio > niceRatio)
                {
                    action = 1;
                }
                else if (curiousityRatio > huntRatio && curiousityRatio > niceRatio && curiousityRatio > aggressionRatio)
                {
                    action = 0;
                }
                else if (huntRatio > curiousityRatio && huntRatio > aggressionRatio && huntRatio > niceRatio)
                {
                    action = 2;
                }
                else
                {
                    action = 3;
                }
            }

            if (action == 0)
            {
                Search();
            }
            else if (action == 1)
            {
                Attack();
            }
            else if (action == 2)
            {
                Hunt();
            }
            else if (action == 3)
            {
                Nice();
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Water")
        {
            water++;
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.tag == "Food")
        {
            food++;
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.tag == "NPC")
        {
            HandleNPCInteraction(collision);
        }
    }

    void HandleNPCInteraction(Collision collision)
    {
        if (aggressionRatio < niceRatio)
        {
            if (food > water)
            {
                food = food / 2;
                collision.gameObject.GetComponent<NPCLogic>().food += food;
            }
            else if (water > food)
            {
                water = water / 2;
                collision.gameObject.GetComponent<NPCLogic>().water += water;
            }
        }
        else
        {
            NPCLogic enemyLogic = collision.gameObject.GetComponent<NPCLogic>();

            if(enemyLogic.size > size)
            {
                enemyLogic.water += water / 2;
                enemyLogic.food += food / 2;
                size -= enemyLogic.size;
                Die();
            }
        }
    }

    GameObject Closest(string tag)
    {
        GameObject[] gos;

        gos = GameObject.FindGameObjectsWithTag(tag);

        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float currentDistance = diff.sqrMagnitude;
            if (currentDistance < distance)
            {
                //the NPC doing the check is closest to itself every time
                if (go != gameObject)
                {
                    closest = go;
                    distance = currentDistance;
                }
            }
        }

        if(closest == null)
        {
            //entire gameworld empty of thing
            closest = gameObject;
        }

        return closest;
    }

    void Die()
    {
        string data = "Food:" + food + "\nWater:" + water + "\nAge:" + age + "\nHealth:" + health + "\nSize:" + size + "\nAggression:" + aggressionRatio + "\nCuriousity:" + curiousityRatio + "\nGenerousity:" + niceRatio;
        Debug.Log(data);
        GameObject[] gos = GameObject.FindGameObjectsWithTag("NPC");
        GameObject biggest = gameObject; //"fittest"
        foreach(GameObject go in gos)
        {
            if(go.transform.localScale.x > biggest.transform.localScale.x)
            {
                biggest = go;
            }
        }

        NPCLogic biglogic = biggest.GetComponent<NPCLogic>();

        float newSize = (biglogic.size + size) / 2;
        float newAggressionRatio = (biglogic.aggressionRatio + aggressionRatio) / 2;
        float newCuriousityRatio = (biglogic.curiousityRatio + curiousityRatio) / 2;
        float newHuntRatio = (biglogic.huntRatio + huntRatio) / 2;
        float newNiceRatio = (biglogic.niceRatio + niceRatio) / 2;

        //prevents infinite loops if NPC size is greater than worldsize
        if (age > 10)
        {
            GameObject.FindGameObjectWithTag("GameMaster").GetComponent<King>().Clone(newSize, newAggressionRatio, newCuriousityRatio, newHuntRatio, newNiceRatio, biglogic.generation, biglogic.age);
        }
        Destroy(gameObject);
    }

    void Search()
    {
        //Drunk walk
        double choice = random.NextDouble();

        if(choice <= 0.25)
        {
            MoveMeTowards(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y, gameObject.transform.position.z));
        }
        else if(choice <= 0.5)
        {
            MoveMeTowards(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y, gameObject.transform.position.z));
        }
        else if(choice <= 0.75)
        {
            MoveMeTowards(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + 1));
        }
        else
        {
            MoveMeTowards(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - 1));
        }

    }

    void Attack()
    {
        MoveMeTowards(Closest("NPC").transform.position);
    }

    void Hunt()
    {
        if (water > food)
        {
            MoveMeTowards(Closest("Food").transform.position);
        }
        else
        {
            MoveMeTowards(Closest("Water").transform.position);
        }
    }

    void Nice()
    {
        MoveMeTowards(Closest("NPC").transform.position);
    }

    public void MoveMeTowards(Vector3 target)
    {
        float sizeRate = size;

        if(sizeRate > 10)
        {
            sizeRate = 1;
        }
        else
        {
            sizeRate = 11 - size;
        }
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target, sizeRate * Time.deltaTime);
        Physics.Raycast(gameObject.transform.position, target, (target - gameObject.transform.position).sqrMagnitude);
    }
}
